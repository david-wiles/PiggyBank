PiggyBank: a financial management system. 

Written in C++ with Qt.

In order to build this project from source, ensure that you have CMake and Qt installed, and be sure to set Qt5_DIR in the CMAKE cache to the location of Qt5Config.cmake.

Once this is done, use the commands ```cmake .``` followed by ```make``` in the project directory to build the executable. 