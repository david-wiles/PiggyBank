#ifndef MAINWINDOW_HXX
#define MAINWINDOW_HXX

#include <QMainWindow>
#include <QtSql/QSqlDatabase>
#include <inc/model/accountmodel.hxx>
#include <inc/model/assetmodel.hxx>
#include <inc/model/budgetmodel.hxx>
#include <inc/model/debtmodel.hxx>
#include <inc/model/incomemodel.hxx>
#include <inc/model/spendingmodel.hxx>
#include <inc/model/personmodel.hxx>
#include <inc/model/investmentmodel.hxx>


/**
 * Main window and application manager. Should be used once, in main.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QSqlDatabase * db, QWidget *parent = nullptr);
    ~MainWindow() override = default;

private:
    QSqlDatabase * database;
    QTabWidget * viewer;

    // Models
    AccountModel * account_model;
    AssetModel * asset_model;
    BudgetModel * budget_model;
    DebtModel * debt_model;
    RecurringIncomeModel * recurring_income_model;
    OneTimeIncomeModel * one_time_income_model;
    SpendingModel * spending_model;
    PersonModel * person_model;
    SecurityInvestmentModel * security_investment_model;

private slots:

    void new_db();
    void load_db();
    void export_db();

    void chart_income();
    void chart_investments();
    void chart_spending();
    void chart_loans();
    void chart_income_spending();

private:
    void setup();

};
#endif // MAINWINDOW_HXX
