#ifndef PIGGYBANK_TABLETAB_HXX
#define PIGGYBANK_TABLETAB_HXX

#include <QWidget>
#include <QtSql/QSqlDatabase>
#include <QtWidgets/QTableView>

#include <inc/model/abstractmodel.hxx>
#include <inc/view/abstractcreatedialog.hxx>
#include <inc/model/accountmodel.hxx>
#include <inc/model/assetmodel.hxx>
#include <inc/model/budgetmodel.hxx>
#include <inc/model/debtmodel.hxx>
#include <inc/model/incomemodel.hxx>
#include <inc/model/investmentmodel.hxx>
#include <inc/model/spendingmodel.hxx>
#include <inc/model/personmodel.hxx>

/**
 * Controller encapsulating a model and its tab view, along with creation, deleting, and update methods.
 * Interaction with the controller from a parent widget only requires that the widget (via get_widget())
 * be added to a QTabWidget in the main widget.
 */
class AbstractTableTab : public QObject
{
    Q_OBJECT
public:
    ~AbstractTableTab() override = default;

    QWidget * get_widget();

protected:
    AbstractTableTab(QWidget * parent, AbstractModel * model);

private:

    QWidget * tab_widget;
    QTableView * table;
    AbstractModel * model;

    void setup();

private slots:

    void new_record();
    void delete_record();
};


class AccountTableTab : public AbstractTableTab
{
    Q_OBJECT
public:
    AccountTableTab(QWidget * parent, AccountModel * model)
        : AbstractTableTab(parent, model) {}
    ~AccountTableTab() override = default;
};

class AssetTableTab : public AbstractTableTab
{
    Q_OBJECT
public:
    AssetTableTab(QWidget * parent, AssetModel * model)
            : AbstractTableTab(parent, model) {}
    ~AssetTableTab() override = default;
};

class BudgetTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    BudgetTableTab(QWidget * parent, BudgetModel * model)
            : AbstractTableTab(parent, model) {}
    ~BudgetTableTab() override = default;
};

class DebtTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    DebtTableTab(QWidget * parent, DebtModel * model)
            : AbstractTableTab(parent, model) {}
    ~DebtTableTab() override = default;
};

class RecurringIncomeTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    RecurringIncomeTableTab(QWidget * parent, RecurringIncomeModel * model)
            : AbstractTableTab(parent, model) {}
    ~RecurringIncomeTableTab() override = default;
};

class OneTimeIncomeTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    OneTimeIncomeTableTab(QWidget * parent, OneTimeIncomeModel * model)
            : AbstractTableTab(parent, model) {}
    ~OneTimeIncomeTableTab() override = default;
};

class SecurityInvestmentTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    SecurityInvestmentTableTab(QWidget * parent, SecurityInvestmentModel * model)
            : AbstractTableTab(parent, model) {}
    ~SecurityInvestmentTableTab() override = default;
};

class PersonTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    PersonTableTab(QWidget * parent, PersonModel * model)
            : AbstractTableTab(parent, model) {}
    ~PersonTableTab() override = default;
};

class SpendingTableTab : public AbstractTableTab
{
Q_OBJECT
public:
    SpendingTableTab(QWidget * parent, SpendingModel * model)
            : AbstractTableTab(parent, model) {}
    ~SpendingTableTab() override = default;
};


#endif //PIGGYBANK_TABLETAB_HXX
