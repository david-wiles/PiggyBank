#ifndef PIGGYBANK_ABSTRACTMODEL_HXX
#define PIGGYBANK_ABSTRACTMODEL_HXX


#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlDatabase>
#include <inc/view/abstractcreatedialog.hxx>


/**
 * Class which associates a model with a creation dialog.
 * Subclasses should set their table name and edit strategy in their constructor. Subclasses should also implement
 * new_dialog() and insert().
 *
 * new_dialog() should execute a creation dialog for that model, and insert should insert a record into the database.
 */
class AbstractModel : public QSqlTableModel
{
public:
    AbstractModel(QObject * parent, QSqlDatabase * database) : QSqlTableModel(parent, *database) {}
    ~AbstractModel() override = default;

    virtual void new_dialog() = 0;
    // insert();
};


#endif //PIGGYBANK_ABSTRACTMODEL_HXX
