#ifndef PIGGYBANK_ABSTRACTTABLEVIEW_HXX
#define PIGGYBANK_ABSTRACTTABLEVIEW_HXX

#include <QtSql/QSqlTableModel>
#include <QtCore/QDate>


/**
 * Parent class for database views used in creation of charts. Subclasses should be related to objects which are
 * related to years, either individually or spanning multiple. Subclasses should only override set_record, which will
 * set what fields are inserted into the table.
 *
 * When an object is persisted to the database, it should call update() if the object is only related to a single year
 * or update_years() if it spans multiple.
 */
class AbstractTableView : public QSqlTableModel
{
public:
    AbstractTableView(QObject * parent, QSqlDatabase * database) : QSqlTableModel(parent, *database) {}
    ~AbstractTableView() override = default;

    virtual void set_record(QSqlRecord* rec, int year, const QString& type, double amt) = 0;
    bool update(int year, const QString & type, double amt);
    bool update_years(QDate start_date, QDate end_date, const QString& type, double total_amt);
};

#endif //PIGGYBANK_ABSTRACTTABLEVIEW_HXX
