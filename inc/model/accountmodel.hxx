#ifndef PIGGYBANK_ACCOUNTMODEL_HXX
#define PIGGYBANK_ACCOUNTMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include "abstractmodel.hxx"


struct account
{
    account() = default;
    account(QString acct_num, QString acct_type, double bal, double inst, QString bank, QString p)
        : account_number(std::move(acct_num)), account_type(std::move(acct_type)),
        balance(bal), interest(inst), institution(std::move(bank)), owner(std::move(p)) {};
    ~account() = default;

    QString account_number;
    QString account_type;
    double balance{};
    double interest{};
    QString institution;
    QString owner;
};

class AccountModel : public AbstractModel
{
public:
    AccountModel(QObject * parent, QSqlDatabase *db);
    ~AccountModel() override = default;

    bool insert(const account &acct);
    void new_dialog() override;
};


#endif //PIGGYBANK_ACCOUNTMODEL_HXX
