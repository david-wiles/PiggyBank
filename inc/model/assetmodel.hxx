#ifndef PIGGYBANK_ASSETMODEL_HXX
#define PIGGYBANK_ASSETMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>

#include "abstractmodel.hxx"


struct asset
{
    asset() = default;
    asset(QString name, QString type, QString owner, double value)
        : name(std::move(name)), type(std::move(type)), owner(std::move(owner)), value(value) {}

    ~asset() = default;

    QString name;
    QString type;
    QString owner;
    double value;
};

class AssetModel : public AbstractModel
{
public:
    AssetModel(QObject *parent, QSqlDatabase * db);
    ~AssetModel() override = default;

    bool insert(const asset& asset);
    void new_dialog() override;
};


#endif //PIGGYBANK_ASSETMODEL_HXX
