#ifndef PIGGYBANK_BUDGETMODEL_HXX
#define PIGGYBANK_BUDGETMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include "abstractmodel.hxx"


struct budget
{
    budget(QString type, double target_amount, QString owner, QString description)
        : type(std::move(type)), target_amount(target_amount), owner(std::move(owner)), description(std::move(description))
    {}
    ~budget() = default;

    QString type;
    double target_amount;
    QString owner;
    QString description;
};

class BudgetModel : public AbstractModel
{
public:
    BudgetModel(QObject * parent, QSqlDatabase * db);
    ~BudgetModel() override = default;

    bool insert(const budget &budget);
    void new_dialog() override;

};


#endif //PIGGYBANK_BUDGETMODEL_HXX
