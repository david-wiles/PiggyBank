#ifndef PIGGYBANK_DEBTMODEL_HXX
#define PIGGYBANK_DEBTMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include <QtCore/QDate>
#include "abstractmodel.hxx"
#include "abstracttableview.hxx"


struct debt
{
    debt() = default;
    debt(double principal, double interest, QString type, const QDate &start_date, const QDate &maturity_date,
         QString description, QString owner)
            :
            principal(principal), interest(interest), type(std::move(type)), start_date(start_date),
            maturity_date(maturity_date), description(std::move(description)), owner(std::move(owner)) {}

    ~debt() = default;

    double principal;
    double interest;
    QString type;
    QDate start_date;
    QDate maturity_date;
    QString description;
    QString owner;
};


class YearlyDebtView : public AbstractTableView
{
public:
    YearlyDebtView(QObject * parent, QSqlDatabase * db);
    ~YearlyDebtView() override = default;

    void set_record(QSqlRecord * rec, int year, const QString& type, double amt) override;

    static YearlyDebtView * instance;
};

class DebtModel : public AbstractModel
{
public:
    DebtModel(QObject *parent, QSqlDatabase *db);
    ~DebtModel() override = default;

    bool insert(const debt& debt);
    void new_dialog() override;

};


#endif //PIGGYBANK_DEBTMODEL_HXX
