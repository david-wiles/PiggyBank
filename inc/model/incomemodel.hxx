#ifndef PIGGYBANK_INCOMEMODEL_HXX
#define PIGGYBANK_INCOMEMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include <QtCore/QDate>
#include "abstractmodel.hxx"
#include "abstracttableview.hxx"


struct recurring_income
{
    recurring_income(QString type, double amount, double hours, int pay_frequency, const QDate &start_date,
                     const QDate &end_date, QString owner)
        : type(std::move(type)), hours(hours), amount(amount),
        pay_frequency(pay_frequency), start_date(start_date), end_date(end_date), owner(std::move(owner)) {}
    ~recurring_income() = default;

    QString type;
    double amount;
    double hours;
    int pay_frequency;
    QDate start_date;
    QDate end_date;
    QString owner;
};

struct one_time_income
{
    one_time_income(QString type, double amount, const QDate &date, QString description, QString owner)
        : type(std::move(type)), amount(amount), date(date), descriptor(std::move(description)), owner(std::move(owner))
    {}
    ~one_time_income() = default;

    QString type;
    double amount;
    QDate date;
    QString descriptor;
    QString owner;
};


class RecurringIncomeModel : public AbstractModel
{
public:
    RecurringIncomeModel(QObject * parent, QSqlDatabase * db);
    ~RecurringIncomeModel() override = default;

    bool insert(const recurring_income& income);
    void new_dialog() override;

};

class OneTimeIncomeModel : public AbstractModel
{
public:
    OneTimeIncomeModel(QObject * parent, QSqlDatabase * db);
    ~OneTimeIncomeModel() override = default;

    bool insert(const one_time_income& income);
    void new_dialog() override;

};

class YearlyIncomeView : public AbstractTableView
{
public:
    YearlyIncomeView(QObject *parent, QSqlDatabase *db);
    ~YearlyIncomeView() override = default;

    void set_record(QSqlRecord * rec, int year, const QString& type, double amt) override;

    static YearlyIncomeView *instance;
};

#endif //PIGGYBANK_INCOMEMODEL_HXX
