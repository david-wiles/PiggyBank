#ifndef PIGGYBANK_INVESTMENTMODEL_HXX
#define PIGGYBANK_INVESTMENTMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include <QtCore/QDate>
#include "abstractmodel.hxx"


struct security_investment
{
    security_investment(QString type, QString ticker, const QDate &buy_date, double buy_price, double num_shares,
                        QString owner)
            :
            type(std::move(type)), ticker(std::move(ticker)), buy_date(buy_date), buy_price(buy_price),
            num_shares(num_shares), owner(std::move(owner))
    {}
    ~security_investment() = default;

    QString type;
    QString ticker;
    QDate buy_date;
    double buy_price;
    double num_shares;
    QString owner;
};


class SecurityInvestmentModel : public AbstractModel
{
public:
    SecurityInvestmentModel(QObject *parent, QSqlDatabase * db);
    ~SecurityInvestmentModel() override = default;

    bool insert(const security_investment& investment);
    void new_dialog() override;

    static QVector<QString> investment_types;
};

#endif //PIGGYBANK_INVESTMENTMODEL_HXX
