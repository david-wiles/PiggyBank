#ifndef PIGGYBANK_PERSONMODEL_HXX
#define PIGGYBANK_PERSONMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include "abstractmodel.hxx"


struct person
{
    person() = default;
    explicit person(QString name, QString first_name, QString last_name, int age)
        : name(std::move(name)), first_name(std::move(first_name)), last_name(std::move(last_name)), age(age) {}
    ~person() = default;

    QString name;
    QString first_name;
    QString last_name;
    int age;
};

class PersonModel : public AbstractModel
{
public:
    PersonModel(QObject *parent, QSqlDatabase * db);
    ~PersonModel() override = default;

    bool insert(const person& person);
    void new_dialog() override;

};


#endif //PIGGYBANK_PERSONMODEL_HXX
