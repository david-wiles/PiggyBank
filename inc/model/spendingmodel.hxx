#ifndef PIGGYBANK_SPENDINGMODEL_HXX
#define PIGGYBANK_SPENDINGMODEL_HXX

#include <utility>

#include <QtSql/QSqlTableModel>
#include <QtCore/QDate>

#include "abstractmodel.hxx"

struct spending
{
    spending() = default;
    spending(const QDate &date, double amount, QString type, QString description, QString owner)
            : date(date), amount(amount), type(std::move(type)), description(std::move(description)), owner(std::move(owner)) {}
    ~spending() = default;

    QDate date;
    double amount;
    QString type;
    QString description;
    QString owner;
};


class SpendingModel : public AbstractModel
{
public:
    SpendingModel(QObject *parent, QSqlDatabase * db);
    ~SpendingModel() override = default;

    bool insert(const spending& spending);
    void new_dialog() override;

    static QVector<QString> spending_types;
};

#endif //PIGGYBANK_SPENDINGMODEL_HXX
