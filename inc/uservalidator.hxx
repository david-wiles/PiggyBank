#ifndef PIGGYBANK_USERVALIDATOR_HXX
#define PIGGYBANK_USERVALIDATOR_HXX


#include <QtGui/QValidator>
#include <inc/model/personmodel.hxx>

/**
 * Ensures that a person object exists in the database before attempting to create one.
 */
class UserValidator : public QValidator
{
public:
    UserValidator(QObject * parent, QSqlDatabase db);
    ~UserValidator() override = default;

    QValidator::State validate(QString &input, int &pos) const override;
    void fixup(QString &input) const override;

private:

    PersonModel * person_model;
};


#endif //PIGGYBANK_USERVALIDATOR_HXX
