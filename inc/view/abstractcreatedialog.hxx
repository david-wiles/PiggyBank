#ifndef PIGGYBANK_ABSTRACTCREATEDIALOG_HXX
#define PIGGYBANK_ABSTRACTCREATEDIALOG_HXX

#include <QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLineEdit>
#include <QtSql/QSqlDatabase>


/**
 * Basic interface for a create dialog.
 *
 * create_object() should make a new object based on the items in the form
 * and use a reference to a model to insert the object into the database.
 * setup() should create the form's fields and add them to a widget.
 *
 * accept() should not be overridden in subclasses, it is bound to the dialog's
 * accept button, and calls create_object() before calling QDialog's accept.
 */
class AbstractCreateDialog : public QDialog
{
Q_OBJECT

public:
    explicit AbstractCreateDialog(QWidget *parent = nullptr);
    ~AbstractCreateDialog() override { delete button_box; };

protected:
    virtual void create_object() = 0;
    virtual void setup() = 0;

    QDialogButtonBox * button_box;

protected slots:
    void accept() override;
};

#endif //PIGGYBANK_ABSTRACTCREATEDIALOG_HXX
