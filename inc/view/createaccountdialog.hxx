#ifndef PIGGYBANK_CREATEACCOUNTDIALOG_HXX
#define PIGGYBANK_CREATEACCOUNTDIALOG_HXX

#include <inc/model/accountmodel.hxx>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include "abstractcreatedialog.hxx"


class CreateAccountDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateAccountDialog(AccountModel * model, QWidget *parent = nullptr);
    ~CreateAccountDialog() override;

protected:
    void create_object() override;
    void setup() override;

    AccountModel * account_model;

    QLineEdit * acct_num;
    QComboBox * acct_type;
    QDoubleSpinBox * acct_balance;
    QDoubleSpinBox * acct_interest;
    QLineEdit * acct_institution;
    QLineEdit * acct_owner;
};

#endif //PIGGYBANK_CREATEACCOUNTDIALOG_HXX
