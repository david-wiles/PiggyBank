#ifndef PIGGYBANK_CREATEASSETDIALOG_HXX
#define PIGGYBANK_CREATEASSETDIALOG_HXX


#include <inc/model/assetmodel.hxx>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include "abstractcreatedialog.hxx"

class CreateAssetDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateAssetDialog(AssetModel * model, QWidget * parent = nullptr);
    ~CreateAssetDialog() override;

protected:
    void create_object() override;
    void setup() override;

    AssetModel * asset_model;

    QLineEdit * name;
    QComboBox * type;
    QLineEdit * owner;
    QDoubleSpinBox * value;
};


#endif //PIGGYBANK_CREATEASSETDIALOG_HXX
