#ifndef PIGGYBANK_CREATEBUDGETDIALOG_HXX
#define PIGGYBANK_CREATEBUDGETDIALOG_HXX


#include <inc/model/budgetmodel.hxx>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include "abstractcreatedialog.hxx"

class CreateBudgetDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateBudgetDialog(BudgetModel * model, QWidget * parent = nullptr);
    ~CreateBudgetDialog() override;

protected:
    void create_object() override;
    void setup() override;

    BudgetModel * budget_model;

    QComboBox * type;
    QDoubleSpinBox * target_amount;
    QLineEdit * owner;
    QLineEdit * description;
};


#endif //PIGGYBANK_CREATEBUDGETDIALOG_HXX
