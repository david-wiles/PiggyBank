#ifndef PIGGYBANK_CREATEDEBTDIALOG_HXX
#define PIGGYBANK_CREATEDEBTDIALOG_HXX

#include <inc/model/debtmodel.hxx>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QComboBox>

#include "abstractcreatedialog.hxx"


class CreateDebtDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateDebtDialog(DebtModel * model, QWidget *parent = nullptr);
    ~CreateDebtDialog() override;

protected:
    void create_object() override;
    void setup() override;

    DebtModel * debt_model;

    QDoubleSpinBox * principal;
    QDoubleSpinBox * interest;
    QComboBox * type;
    QDateEdit * start_date;
    QDateEdit * maturity_date;
    QLineEdit * description;
    QLineEdit * owner;
};


#endif //PIGGYBANK_CREATEDEBTDIALOG_HXX
