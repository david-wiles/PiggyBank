#ifndef PIGGYBANK_CREATEINCOMEDIALOG_HXX
#define PIGGYBANK_CREATEINCOMEDIALOG_HXX

#include <inc/model/incomemodel.hxx>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QDateEdit>
#include "abstractcreatedialog.hxx"


class CreateRecurringIncomeDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateRecurringIncomeDialog(RecurringIncomeModel * model, QWidget *parent = nullptr);
    ~CreateRecurringIncomeDialog() override;

protected:
    void create_object() override;
    void setup() override;

    RecurringIncomeModel * income_model;

    QComboBox * type;
    QDoubleSpinBox * hours;
    QDoubleSpinBox * amount;
    QSpinBox * pay_frequency;
    QDateEdit * start_date;
    QDateEdit * end_date;
    QLineEdit * owner;
};


class CreateOneTimeIncomeDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateOneTimeIncomeDialog(OneTimeIncomeModel * model, QWidget * parent = nullptr);
    ~CreateOneTimeIncomeDialog() override;

protected:
    void create_object() override;
    void setup() override;

    OneTimeIncomeModel * income_model;

    QComboBox * type;
    QDoubleSpinBox * amount;
    QDateEdit * date;
    QLineEdit * descriptor;
    QLineEdit * owner;
};

#endif //PIGGYBANK_CREATEINCOMEDIALOG_HXX
