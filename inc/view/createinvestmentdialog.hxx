#ifndef PIGGYBANK_CREATEINVESTMENTDIALOG_HXX
#define PIGGYBANK_CREATEINVESTMENTDIALOG_HXX

#include <inc/model/investmentmodel.hxx>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDoubleSpinBox>
#include "abstractcreatedialog.hxx"


class CreateInvestmentDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateInvestmentDialog(SecurityInvestmentModel * model, QWidget *parent = nullptr);
    ~CreateInvestmentDialog() override;

protected:
    void create_object() override;
    void setup() override;

    SecurityInvestmentModel * investment_model;

    QComboBox * type;
    QLineEdit * ticker;
    QDateEdit * buy_date;
    QDoubleSpinBox * buy_price;
    QDoubleSpinBox * num_shares;
    QLineEdit * owner;
};

#endif //PIGGYBANK_CREATEINVESTMENTDIALOG_HXX
