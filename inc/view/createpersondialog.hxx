#ifndef PIGGYBANK_CREATEPERSONDIALOG_HXX
#define PIGGYBANK_CREATEPERSONDIALOG_HXX

#include <QtWidgets/QSpinBox>

#include <inc/model/personmodel.hxx>
#include "abstractcreatedialog.hxx"


class CreatePersonDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreatePersonDialog(PersonModel * model, QWidget * parent = nullptr);
    ~CreatePersonDialog() override;

protected:
    void create_object() override;
    void setup() override;

    PersonModel * person_model;

    QLineEdit * name;
    QLineEdit * first_name;
    QLineEdit * last_name;
    QSpinBox * age;
};


#endif //PIGGYBANK_CREATEPERSONDIALOG_HXX
