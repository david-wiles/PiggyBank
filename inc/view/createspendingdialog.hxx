#ifndef PIGGYBANK_CREATESPENDINGDIALOG_HXX
#define PIGGYBANK_CREATESPENDINGDIALOG_HXX

#include <inc/model/spendingmodel.hxx>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QComboBox>
#include "abstractcreatedialog.hxx"


class CreateSpendingDialog : public AbstractCreateDialog
{
    Q_OBJECT

public:
    explicit CreateSpendingDialog(SpendingModel * model, QWidget *parent = nullptr);
    ~CreateSpendingDialog() override;

protected:
    void create_object() override;
    void setup() override;

    SpendingModel * spending_model;

    QDateEdit * date;
    QDoubleSpinBox * amount;
    QComboBox * type;
    QLineEdit * description;
    QLineEdit * owner;
};

#endif //PIGGYBANK_CREATESPENDINGDIALOG_HXX
