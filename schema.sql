create table if not exists account
(
    account_number text not null
        constraint account_pk
            primary key,
    account_type text not null,
    balance   decimal default 0 not null,
    interest   decimal default 0 not null,
    institution text not null,
    owner     text    not null
);

create unique index if not exists account_acct_num_uindex
    on account (account_number);

create table if not exists person
(
    dummy text,
    username text not null
        constraint auth_user_pk
            primary key,
    first_name text,
    last_name text,
    age integer
);

create unique index if not exists auth_user_username_uindex
    on person (username);

create table if not exists asset
(
    name text not null
        constraint asset_pk
            primary key,
    owner text    not null
        references person
            on update cascade on delete cascade,
    type  text    not null,
    value decimal default 0 not null
);

create unique index if not exists asset_id_uindex
    on asset (name);

create table if not exists budget
(
    id integer not null
        constraint budget_pk
            primary key autoincrement,
    type text not null,
    target_amount decimal not null,
    owner text not null
        references person
            on update cascade on delete cascade,
    description text
);

create unique index if not exists budget_id_uindex
    on budget (id);

create table if not exists debt
(
    id integer not null
        constraint debt_pk
            primary key autoincrement,
    principal     decimal   not null,
    interest      decimal   not null,
    type          text      not null,
    start_date    date not null,
    maturity_date date not null,
    description text,
    owner text not null
        references person
            on update cascade on delete cascade
);

create unique index if not exists debt_id_uindex
    on debt (id);

create table if not exists recurring_income
(
    id            integer not null
        constraint income_pk
            primary key autoincrement,
    type text not null,
    amount        decimal not null,
    hours         decimal default 0 not null,
    pay_frequency integer default 1 not null,
    start_date date not null,
    end_date date not null,
    owner         text    not null
        references person
            on update cascade on delete cascade
);

create unique index if not exists r_income_id_uindex
    on recurring_income (id);

create table if not exists one_time_income
(
    id            integer not null
        constraint income_pk
            primary key autoincrement,
    type text not null,
    amount        decimal not null,
    date date not null,
    descriptor text,
    owner         text    not null
        references person
            on update cascade on delete cascade
);

create unique index if not exists ot_income_id_uindex
    on one_time_income (id);

create table if not exists yearly_income_view
(
    year integer not null
        constraint yearly_income_pk
            primary key,
    Salary decimal default 0.0,
    Hourly decimal default 0.0,
    Rental decimal default 0.0,
    Dividend decimal default 0.0,
    Bond decimal default 0.0
);

create unique index if not exists yearly_income_uindex
    on yearly_income_view (year);

create table if not exists investment
(
    id         integer not null
        constraint investment_pk
            primary key autoincrement,
    type       text    not null,
    ticker     text    not null,
    buy_date   date not null,
    buy_price  decimal not null,
    num_shares decimal not null,
    owner      text    not null
        references person
            on update cascade on delete cascade
);

create unique index if not exists investment_id_uindex
    on investment (id);

create table if not exists spending
(
    id integer not null
        constraint spending_pk
            primary key autoincrement,
    date date not null,
    amount decimal default 0.0,
    type text not null,
    description text,
    owner text not null
        references person
            on update cascade on delete cascade
);

create unique index if not exists spending_id_uindex
    on spending (id);

create table if not exists yearly_debt_view
(
    year integer not null
        constraint yearly_income_pk
            primary key,
    Personal decimal default 0.0,
    Mortgage decimal default 0.0,
    Credit decimal default 0.0,
    Car decimal default 0.0,
    Student decimal default 0.0
);

create unique index if not exists yearly_debt_view_uindex
    on yearly_debt_view (year);