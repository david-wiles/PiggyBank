#include "inc/controller/mainwindow.hxx"

#include <QtSql/QSqlRecord>

#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QStackedBarSeries>
#include <QtCharts/QPieSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QVBarModelMapper>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>

#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>

#include <inc/controller/tabletab.hxx>
#include <inc/model/personmodel.hxx>
#include <inc/model/budgetmodel.hxx>
#include <inc/model/assetmodel.hxx>
#include <QtCore/QTextStream>
#include <QtSql/QSqlQuery>


MainWindow::MainWindow(QSqlDatabase * db, QWidget *parent) : QMainWindow(parent), database(db)
{
    account_model = new AccountModel(this, db);
    asset_model = new AssetModel(this, db);
    budget_model = new BudgetModel(this, db);
    debt_model = new DebtModel(this, db);
    recurring_income_model = new RecurringIncomeModel(this, db);
    one_time_income_model = new OneTimeIncomeModel(this, db);
    spending_model = new SpendingModel(this, db);
    person_model = new PersonModel(this, db);
    security_investment_model = new SecurityInvestmentModel(this, db);
    setup();
}

void MainWindow::setup()
{
    this->resize(1033, 593);

    auto centralwidget = new QWidget(this);
    auto gridLayout = new QGridLayout(centralwidget);
    viewer = new QTabWidget(centralwidget);
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);

    auto actionNew = new QAction(this);
    actionNew->setText("New database");
    auto actionLoad = new QAction(this);
    actionLoad->setText("Load from file");
    auto actionExport = new QAction(this);
    actionExport->setText("Export to file");
    auto actionQuit = new QAction(this);
    actionQuit->setText("Quit");

    auto chartIncome = new QAction(this);
    chartIncome->setText("Income");
    auto chartInvestments = new QAction(this);
    chartInvestments->setText("Investments");
    auto chartSpending = new QAction(this);
    chartSpending->setText("Spending");
    auto chartLoans = new QAction(this);
    chartLoans->setText("Loans");
    auto chartIncomeSpending = new QAction(this);
    chartIncomeSpending->setText("Income and Spending");

    auto menubar = new QMenuBar(this);
    auto menuFiles = new QMenu(menubar);
    menuFiles->setTitle("File");
    auto menuChart = new QMenu(menubar);
    menuChart->setTitle("Chart");
    auto statusbar = new QStatusBar(this);

    auto account_tab = new AccountTableTab(this, account_model);
    auto person_tab = new PersonTableTab(this, person_model);
    auto recurring_income_tab = new RecurringIncomeTableTab(this, recurring_income_model);
    auto asset_tab = new AssetTableTab(this, asset_model);
    auto budget_tab = new BudgetTableTab(this, budget_model);
    auto one_time_income_tab = new OneTimeIncomeTableTab(this, one_time_income_model);
    auto debt_tab = new DebtTableTab(this, debt_model);
    auto spending_tab = new SpendingTableTab(this, spending_model);
    auto security_investment_tab = new SecurityInvestmentTableTab(this, security_investment_model);

    viewer->addTab(person_tab->get_widget(), tr("Persons"));
    viewer->addTab(account_tab->get_widget(), tr("Accounts"));
    viewer->addTab(asset_tab->get_widget(), tr("Assets"));
    viewer->addTab(budget_tab->get_widget(), tr("Budgets"));
    viewer->addTab(recurring_income_tab->get_widget(), tr("Recurring Income"));
    viewer->addTab(one_time_income_tab->get_widget(), tr("One Time Income"));
    viewer->addTab(debt_tab->get_widget(), tr("Debt"));
    viewer->addTab(spending_tab->get_widget(), tr("Spending"));
    viewer->addTab(security_investment_tab->get_widget(), tr("Investment"));

    gridLayout->addWidget(viewer, 0, 1, 1, 1);

    menubar->setGeometry(QRect(0, 0, 1033, 22));
    menubar->addAction(menuFiles->menuAction());
    menubar->addAction(menuChart->menuAction());
    menuFiles->addAction(actionNew);
    menuFiles->addAction(actionLoad);
    menuFiles->addAction(actionExport);
    menuFiles->addAction(actionQuit);
    menuChart->addAction(chartIncome);
    menuChart->addAction(chartInvestments);
    menuChart->addAction(chartSpending);
    menuChart->addAction(chartLoans);
    menuChart->addAction(chartIncomeSpending);

    this->setCentralWidget(centralwidget);
    this->setMenuBar(menubar);
    this->setStatusBar(statusbar);

    QObject::connect(actionNew, SIGNAL(triggered()), this, SLOT(new_db()));
    QObject::connect(actionLoad, SIGNAL(triggered()), this, SLOT(load_db()));
    QObject::connect(actionExport, SIGNAL(triggered()), this, SLOT(export_db()));
    QObject::connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));

    QObject::connect(chartIncome, SIGNAL(triggered()), this, SLOT(chart_income()));
    QObject::connect(chartInvestments, SIGNAL(triggered()), this, SLOT(chart_investments()));
    QObject::connect(chartSpending, SIGNAL(triggered()), this, SLOT(chart_spending()));
    QObject::connect(chartLoans, SIGNAL(triggered()), this, SLOT(chart_loans()));
    QObject::connect(chartIncomeSpending, SIGNAL(triggered()), this, SLOT(chart_income_spending()));
}

void MainWindow::new_db()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Select database"), ".", tr("Database Files (*.db *.sqlite)"));
    database->setDatabaseName(filename);

    QStringList lines;
    QFile file("init.sql");
    file.open(QIODevice::Text | QIODevice::ReadOnly);
    QTextStream stream(&file);
    while (!file.atEnd())
        lines.append(file.readLine());
    QSqlQuery query(*database);

    foreach (QString sql, lines) {
        query.exec(sql);
    }

    delete account_model;
    delete asset_model;
    delete budget_model;
    delete debt_model;
    delete recurring_income_model;
    delete one_time_income_model;
    delete spending_model;
    delete person_model;
    delete security_investment_model;

    account_model = new AccountModel(this, database);
    asset_model = new AssetModel(this, database);
    budget_model = new BudgetModel(this, database);
    debt_model = new DebtModel(this, database);
    recurring_income_model = new RecurringIncomeModel(this, database);
    one_time_income_model = new OneTimeIncomeModel(this, database);
    spending_model = new SpendingModel(this, database);
    person_model = new PersonModel(this, database);
    security_investment_model = new SecurityInvestmentModel(this, database);

    setup();
}

void MainWindow::load_db()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Select database"), ".", tr("Database Files (*.db *.sqlite)"));
    if (!filename.isEmpty()) {
        this->hide();
        database->setDatabaseName(filename);
        if (!database->open()) {
            QMessageBox::critical(this, QObject::tr("Cannot open database"),
                                  QObject::tr("Try loading another file."), QMessageBox::Cancel);
        }
        delete account_model;
        delete asset_model;
        delete budget_model;
        delete debt_model;
        delete recurring_income_model;
        delete one_time_income_model;
        delete spending_model;
        delete person_model;
        delete security_investment_model;

        account_model = new AccountModel(this, database);
        asset_model = new AssetModel(this, database);
        budget_model = new BudgetModel(this, database);
        debt_model = new DebtModel(this, database);
        recurring_income_model = new RecurringIncomeModel(this, database);
        one_time_income_model = new OneTimeIncomeModel(this, database);
        spending_model = new SpendingModel(this, database);
        person_model = new PersonModel(this, database);
        security_investment_model = new SecurityInvestmentModel(this, database);

        setup();
        this->show();
    }
}

void MainWindow::export_db()
{
    QString new_db = QFileDialog::getOpenFileName(this, tr("Select file"), ".", tr("Database Files (*.db *.sqlite)"));
    if (!new_db.isEmpty()) {
        QFile::copy(database->connectionName(), new_db);
        QMessageBox::information(this, QObject::tr("Success!"), QObject::tr("Database successfully exported."));
    }
}

void MainWindow::chart_income()
{
    YearlyIncomeView::instance->select();

    auto chart = new QtCharts::QChart;
    chart->setAnimationOptions(QtCharts::QChart::AllAnimations);
    auto series = new QtCharts::QStackedBarSeries(chart);

    auto mapper = new QtCharts::QVBarModelMapper(this);
    mapper->setFirstBarSetColumn(1);
    mapper->setLastBarSetColumn(5);
    mapper->setFirstRow(0);
    mapper->setRowCount(YearlyIncomeView::instance->rowCount());
    mapper->setSeries(series);
    mapper->setModel(YearlyIncomeView::instance);
    chart->addSeries(series);

    QStringList cats;
    for (int i = 0; i < YearlyIncomeView::instance->rowCount(); ++i) {
        cats << QString::number(
                YearlyIncomeView::instance->record(i)
                .value("year")
                .toInt()
                );
    }

    auto axisX = new QtCharts::QBarCategoryAxis(chart);
    axisX->append(cats);
    axisX->setTitleText("Year");
    auto axisY = new QtCharts::QValueAxis(chart);
    axisY->setTitleText("Amount (USD)");
    chart->createDefaultAxes();
    chart->setAxisX(axisX, series);
    chart->setTitle("Income amount per year");

    auto chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setMinimumSize(640, 480);
    chartView->show();
}

void MainWindow::chart_investments()
{
    auto chart = new QtCharts::QChart;
    chart->setAnimationOptions(QtCharts::QChart::AllAnimations);
    auto pieSeries = new QtCharts::QPieSeries(chart);

    QMap<QString, double> slices;

    for (int i = 0; i < security_investment_model->rowCount(); ++i) {
        QModelIndex type_index(security_investment_model->index(i, 1));
        QModelIndex buy_price_index(security_investment_model->index(i, 4));
        QModelIndex num_shares_index(security_investment_model->index(i, 5));

        QString type = security_investment_model->data(type_index).toString();
        double buy_price = security_investment_model->data(buy_price_index).toDouble();
        double num_shares = security_investment_model->data(num_shares_index).toDouble();

        if (!slices.contains(type)) {
            slices[type] = buy_price * num_shares;
        } else {
           double amt = slices[type];
           slices.insert(type, amt + (buy_price * num_shares));
        }
    }

    for (QMap<QString, double>::iterator itr = slices.begin(); itr != slices.end(); ++itr) {
        auto slice = new QtCharts::QPieSlice(itr.key(), itr.value(), pieSeries);
        slice->setLabel(itr.key() + ": $" + QString::number(itr.value()));
        pieSeries->append(slice);
    }

    pieSeries->setLabelsVisible();
    chart->addSeries(pieSeries);
    chart->setTitle("Investment amount");

    auto chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setMinimumSize(640, 480);
    chartView->show();
}

void MainWindow::chart_spending()
{
    auto chart = new QtCharts::QChart();
    chart->setAnimationOptions(QtCharts::QChart::AllAnimations);
    auto barSeries = new QtCharts::QBarSeries(chart);

    // Type x user
    QMap<QString, QMap<QString, double>> types;
    QVector<QString> persons;
    int num_types = 0;
    int num_users = 0;

    for (int i = 0; i < spending_model->rowCount(); ++i) {
        QModelIndex amount_index(spending_model->index(i, 2));
        QModelIndex type_index(spending_model->index(i, 3));
        QModelIndex owner_index(spending_model->index(i, 5));

        double amount = spending_model->data(amount_index).toDouble();
        QString type = spending_model->data(type_index).toString();
        QString owner = spending_model->data(owner_index).toString();

        if (!persons.contains(owner)) {
            persons.append(owner);
            num_users++;
        }

        if (!types.contains(type)) {
            QMap<QString, double> users;
            users.insert(owner, amount);
            types.insert(type, users);
            num_types++;
        } else {
            QMap<QString, double> * users = &types[type];
            if (!users->contains(owner)) {
                users->insert(owner, amount);
            } else {
                double amt = users->value(owner, 0);
                users->insert(owner, amt + amount);
            }
        }

    }

    QStringList cats;
    QMap<QString, QMap<QString, double>>::iterator type_itr = types.begin();
    // Create sets
    QVector<QtCharts::QBarSet*> sets;
    while (type_itr != types.end()) {
        sets.append(new QtCharts::QBarSet(type_itr.key()));
        type_itr++;
    }
    // Set categories
    for (int i = 0; i < num_users; ++i) {
        cats << persons.at(i);
    }

    for (int i = 0; i < num_users; ++i) {
        type_itr = types.begin();
        for (int j = 0; j < num_types; ++j) {
            *sets.at(j) << type_itr.value().value(persons.value(i), 0);
            type_itr++;
        }
    }

    // Append sets to series
    for (int i = 0; i < num_types; ++i) {
        barSeries->append(sets.at(i));
    }

    chart->addSeries(barSeries);
    auto axisX = new QtCharts::QBarCategoryAxis(chart);
    axisX->append(cats);
    axisX->setTitleText("Person");
    auto axisY = new QtCharts::QValueAxis(chart);
    axisY->setTitleText("Amount (USD)");
    chart->setAxisX(axisX, barSeries);
    chart->setTitle("Total Spending Amount");

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    auto chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setMinimumSize(640, 480);
    chartView->show();
}

void MainWindow::chart_loans()
{
    YearlyDebtView::instance->select();

    auto chart = new QtCharts::QChart;
    chart->setAnimationOptions(QtCharts::QChart::AllAnimations);
    auto series = new QtCharts::QStackedBarSeries(chart);

    auto mapper = new QtCharts::QVBarModelMapper(this);
    mapper->setFirstBarSetColumn(1);
    mapper->setLastBarSetColumn(5);
    mapper->setFirstRow(0);
    mapper->setRowCount(YearlyDebtView::instance->rowCount());
    mapper->setSeries(series);
    mapper->setModel(YearlyDebtView::instance);
    chart->addSeries(series);

    QStringList cats;
    for (int i = 0; i < YearlyDebtView::instance->rowCount(); ++i) {
        cats << QString::number(
                YearlyDebtView::instance->record(i)
                        .value("year")
                        .toInt()
        );
    }

    auto axisX = new QtCharts::QBarCategoryAxis(chart);
    axisX->append(cats);
    auto axisY = new QtCharts::QValueAxis(chart);
    axisY->setTitleText("Amount (USD)");
    axisX->setTitleText("Year");
    chart->setAxisX(axisX, series);
    chart->setAxisY(axisY, series);
    chart->setTitle("Loan amount per year");

    auto chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setMinimumSize(640, 480);
    chartView->show();
}

void MainWindow::chart_income_spending()
{

    YearlyIncomeView::instance->select();
    auto chart = new QtCharts::QChart;
    chart->setAnimationOptions(QtCharts::QChart::AllAnimations);
    auto income_series = new QtCharts::QStackedBarSeries(chart);
    auto spending_series = new QtCharts::QStackedBarSeries(chart);

    // Map income series
    auto mapper = new QtCharts::QVBarModelMapper(this);
    mapper->setFirstBarSetColumn(1);
    mapper->setLastBarSetColumn(5);
    mapper->setFirstRow(0);
    mapper->setRowCount(YearlyIncomeView::instance->rowCount());
    mapper->setSeries(income_series);
    mapper->setModel(YearlyIncomeView::instance);

    QStringList cats;
    int min_year = 9999, max_year = 0;
    for (int i = 0; i < YearlyIncomeView::instance->rowCount(); ++i) {
        int year = YearlyIncomeView::instance->record(i)
                .value("year")
                .toInt();
        if (year > max_year)
            max_year = year;
        if (year < min_year)
            min_year = year;
    }
    for (int i = 0; i < YearlyDebtView::instance->rowCount(); ++i) {
        int year = YearlyDebtView::instance->record(i)
                .value("year")
                .toInt();
        if (year > max_year)
            max_year = year;
        if (year < min_year)
            min_year = year;
    }
    for (int y = min_year; y <= max_year; ++y) {
        cats << QString::number(y);
    }

    // Map spending series
    // Type x year
    QMap<QString, QMap<int, double>> types;
    QVector<int> years;
    int num_types = 0;
    int num_years = 0;

    for (int i = 0; i < spending_model->rowCount(); ++i) {
        QModelIndex date_index(spending_model->index(i, 1));
        QModelIndex amount_index(spending_model->index(i, 2));
        QModelIndex type_index(spending_model->index(i, 3));

        QDate date = QDate::fromString(
                spending_model->data(date_index)
                .toString(), "yyyy-MM-dd"
                );
        double amount = spending_model->data(amount_index).toDouble();
        QString type = spending_model->data(type_index).toString();

        if (!years.contains(date.year())) {
            years.append(date.year());
            num_years++;
        }

        if (!types.contains(type)) {
            QMap<int, double> years_map;
            years_map.insert(date.year(), amount);
            types.insert(type, years_map);
            num_types++;
        } else {
            QMap<int, double> * years_map = &types[type];
            if (!years_map->contains(date.year())) {
                years_map->insert(date.year(), amount);
            } else {
                double amt = years_map->value(date.year(), 0);
                years_map->insert(date.year(), amt + amount);
            }
        }

    }

    QMap<QString, QMap<int, double>>::iterator type_itr = types.begin();
    // Create sets
    QVector<QtCharts::QBarSet*> sets;
    while (type_itr != types.end()) {
        sets.append(new QtCharts::QBarSet(type_itr.key()));
        type_itr++;
    }

    for (int i = 0; i < num_years; ++i) {
        type_itr = types.begin();
        for (int j = 0; j < num_types; ++j) {
            *sets.at(j) << type_itr.value().value(years.value(i), 0);
            type_itr++;
        }
    }
    // Append sets to series
    for (int i = 0; i < num_types; ++i) {
        spending_series->append(sets.at(i));
    }

    chart->addSeries(income_series);
    chart->addSeries(spending_series);

    auto axisX = new QtCharts::QBarCategoryAxis(chart);
    axisX->append(cats);
    axisX->setTitleText("Year");
    chart->addAxis(axisX, Qt::AlignBottom);
    income_series->attachAxis(axisX);
    spending_series->attachAxis(axisX);

    auto axisY = new QtCharts::QValueAxis(chart);
    axisY->setTitleText("Amount (USD)");
    chart->addAxis(axisY, Qt::AlignLeft);
    income_series->attachAxis(axisY);
    spending_series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->setTitle("Income and Spending");

    auto chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setMinimumSize(640, 480);
    chartView->show();
}
