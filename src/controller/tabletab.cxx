#include "inc/controller/tabletab.hxx"

#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QTableView>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>


AbstractTableTab::AbstractTableTab(QWidget *parent, AbstractModel *model) : QObject(parent), model(model)
{
    tab_widget = new QWidget(parent);
    setup();
}

QWidget *AbstractTableTab::get_widget()
{
    return tab_widget;
}

void AbstractTableTab::setup()
{
    model->select();
    auto buttons = new QDialogButtonBox(Qt::Vertical);
    auto save_changes = new QPushButton(QObject::tr("Save changes"));
    buttons->addButton(save_changes, QDialogButtonBox::ActionRole);
    auto new_row = new QPushButton(QObject::tr("New"));
    buttons->addButton(new_row, QDialogButtonBox::ActionRole);
    auto delete_row = new QPushButton(QObject::tr("Delete"));
    buttons->addButton(delete_row, QDialogButtonBox::ActionRole);
    table = new QTableView;
    table->setModel(model);
    table->hideColumn(0);
    table->show();
    auto layout = new QHBoxLayout;
    layout->addWidget(table);
    layout->addWidget(buttons);
    tab_widget->setLayout(layout);
    QObject::connect(save_changes, &QPushButton::clicked, model, &AbstractModel::submitAll);
    QObject::connect(new_row, &QPushButton::clicked, this, &AbstractTableTab::new_record);
    QObject::connect(delete_row, &QPushButton::clicked, this, &AbstractTableTab::delete_record);
}

void AbstractTableTab::new_record()
{
    model->new_dialog();
}


void AbstractTableTab::delete_record()
{
    auto select = table->selectionModel();
    if (select->hasSelection()) {
        QList<QModelIndex> rows = select->selectedRows();
        foreach (QModelIndex index, rows) {
            model->removeRow(index.row());
        }
        model->submitAll();
    }
    model->select();
}
