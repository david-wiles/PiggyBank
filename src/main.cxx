#include <QApplication>
#include <QtSql/QSqlDatabase>
#include <QtWidgets/QMessageBox>
#include <QtSql/QSqlQuery>
#include <QtCore/QFile>
#include <QtCore/QTextStream>

#include "inc/controller/mainwindow.hxx"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Initialize database
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("data.db");

    if (!db.open()) {
        QMessageBox::critical(
                nullptr,
                QObject::tr("Cannot open database"),
                QObject::tr("Try loading another file."),
                QMessageBox::Cancel);
    }

    // Read init sql (split into lines) and initialize
    QStringList lines;
    QFile file("init.sql");
    file.open(QIODevice::Text | QIODevice::ReadOnly);
    QTextStream stream(&file);
    while (!file.atEnd())
        lines.append(file.readLine());
    QSqlQuery query(db);
    foreach (QString sql, lines) {
        query.exec(sql);
    }

    MainWindow w{&db, nullptr};
    w.show();
    return a.exec();
}
