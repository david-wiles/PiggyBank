#include "inc/model/abstracttableview.hxx"

#include <QtSql/QSqlRecord>


bool AbstractTableView::update(int year, const QString &type, double amt)
{
    bool ret;
    QSqlRecord rec = record();
    QList<QModelIndex> idx = this->match(this->index(0, 0), Qt::DisplayRole, year, 1, Qt::MatchExactly);

    if (idx.isEmpty()) {
        set_record(&rec, year, type, amt);
        insertRecord(-1, rec);
    } else {
        rec = this->record(idx[0].row());
        double current_val = rec.value(type).toDouble();
        this->setData(this->index(idx[0].row(), fieldIndex(type)), current_val + amt);
    }
    ret = this->submitAll();
    this->select();
    return ret;
}

bool AbstractTableView::update_years(QDate start_date, QDate end_date, const QString &type, double amount)
{
    int start_year = start_date.year();
    int end_year = end_date.year();
    
    if (start_year == end_year) {
        int days = start_date.daysTo(end_date);
        if (!update(start_year, type, amount * ((double) days / 365.0)))
            return false;
    } else {
        // Calculate the amount earned each year
        int days_start = start_date.daysTo(QDate(start_year + 1, start_date.month(), start_date.day()));
        if (!update(start_year, type, amount * ((double) days_start / 365.0)))
            return false;
        while (++start_year < end_year) {
            if (!update(start_year, type, amount))
                return false;
        }
        int days_end = QDate(end_year, 1, 1).daysTo(end_date);
        if (!update(end_year, type, amount * ((double) days_end / 365.0)))
            return false;
    }
}
