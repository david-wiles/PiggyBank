#include "inc/model/accountmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createaccountdialog.hxx>


AccountModel::AccountModel(QObject *parent, QSqlDatabase *db) : AbstractModel(parent, db)
{
    this->setTable("account");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool AccountModel::insert(const account &acct)
{
    QSqlRecord rec = record();
    rec.setValue("account_number", acct.account_number);
    rec.setValue("account_type", acct.account_type);
    rec.setValue("balance", acct.balance);
    rec.setValue("interest", acct.interest);
    rec.setValue("institution", acct.institution);
    rec.setValue("owner", acct.owner);
    return insertRecord(-1, rec);
}

void AccountModel::new_dialog()
{
    CreateAccountDialog dialog{this};
    dialog.exec();
}
