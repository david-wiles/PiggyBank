#include "inc/model/assetmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createassetdialog.hxx>

AssetModel::AssetModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    this->setTable("asset");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool AssetModel::insert(const asset &asset)
{
    QSqlRecord rec = record();
    rec.setValue("name", asset.name);
    rec.setValue("owner", asset.owner);
    rec.setValue("type", asset.type);
    rec.setValue("value", asset.value);
    return insertRecord(-1, rec);
}

void AssetModel::new_dialog()
{
    CreateAssetDialog dialog{this};
    dialog.exec();
}
