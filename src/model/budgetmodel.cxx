#include "inc/model/budgetmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createbudgetdialog.hxx>

BudgetModel::BudgetModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    this->setTable("budget");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool BudgetModel::insert(const budget &budget)
{
    QSqlRecord rec = record();
    rec.setValue("type", budget.type);
    rec.setValue("target_amount", budget.target_amount);
    rec.setValue("owner", budget.owner);
    rec.setValue("description", budget.description);
    return insertRecord(-1, rec);
}

void BudgetModel::new_dialog()
{
    CreateBudgetDialog dialog{this};
    dialog.exec();
}
