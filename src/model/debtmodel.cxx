#include "inc/model/debtmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createdebtdialog.hxx>


DebtModel::DebtModel(QObject *parent, QSqlDatabase *db) : AbstractModel(parent, db)
{
    if (YearlyDebtView::instance == nullptr)
        YearlyDebtView::instance = new YearlyDebtView(parent, db);
    this->setTable("debt");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool DebtModel::insert(const debt &debt)
{
    QSqlRecord rec = record();
    rec.setValue("principal", debt.principal);
    rec.setValue("interest", debt.interest);
    rec.setValue("type", debt.type);
    rec.setValue("start_date", debt.start_date.toString("yyyy-MM-dd"));
    rec.setValue("maturity_date", debt.maturity_date.toString("yyyy-MM-dd"));
    rec.setValue("description", debt.description);
    rec.setValue("owner", debt.owner);

    double year_pct = (double) debt.start_date.daysTo(debt.maturity_date) / 365.0;
    double amount = debt.principal + (debt.principal * debt.interest * year_pct);

    YearlyDebtView::instance->update_years(debt.start_date, debt.maturity_date, debt.type, amount);

    return insertRecord(-1, rec);
}

void DebtModel::new_dialog()
{
    CreateDebtDialog dialog{this};
    dialog.exec();
}


YearlyDebtView * YearlyDebtView::instance = nullptr;

YearlyDebtView::YearlyDebtView(QObject *parent, QSqlDatabase *db) : AbstractTableView(parent, db)
{
    this->setTable("yearly_debt_view");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
    YearlyDebtView::instance = this;
}

void YearlyDebtView::set_record(QSqlRecord *rec, int year, const QString &type, double amt)
{
    rec->setValue("year", year);
    rec->setValue("Personal", 0);
    rec->setValue("Mortgage", 0);
    rec->setValue("Credit", 0);
    rec->setValue("Car", 0);
    rec->setValue("Student", 0);
    rec->setValue(type, amt);
}