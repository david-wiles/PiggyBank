#include "inc/model/incomemodel.hxx"

#include <QtSql/QSqlRecord>
#include <inc/view/createincomedialog.hxx>


RecurringIncomeModel::RecurringIncomeModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    if (YearlyIncomeView::instance == nullptr)
        YearlyIncomeView::instance = new YearlyIncomeView(parent, db);
    this->setTable("recurring_income");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool RecurringIncomeModel::insert(const recurring_income &income)
{
    QSqlRecord rec = record();
    rec.setValue("type", income.type);
    rec.setValue("amount", income.amount);
    rec.setValue("hours", income.hours);
    rec.setValue("pay_frequency", income.pay_frequency);
    rec.setValue("start_date", income.start_date.toString("yyyy-MM-dd"));
    rec.setValue("end_date", income.end_date.toString("yyyy-MM-dd"));
    rec.setValue("owner", income.owner);

    double amount = 0;
    if (income.type == "Salary")
        amount = income.amount;
    else if (income.type == "Hourly")
        amount = income.amount * income.hours * 52;
    else if (income.type == "Rental")
        amount = income.amount * 12;

    YearlyIncomeView::instance->update_years(income.start_date, income.end_date, income.type, amount);

    return insertRecord(-1, rec);
}

void RecurringIncomeModel::new_dialog()
{
    CreateRecurringIncomeDialog dialog{this};
    dialog.exec();
}

OneTimeIncomeModel::OneTimeIncomeModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    if (YearlyIncomeView::instance == nullptr)
        YearlyIncomeView::instance = new YearlyIncomeView(parent, db);
    this->setTable("one_time_income");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool OneTimeIncomeModel::insert(const one_time_income &income)
{
    QSqlRecord rec = record();
    rec.setValue("type", income.type);
    rec.setValue("amount", income.amount);
    rec.setValue("date", income.date.toString("yyyy-MM-dd"));
    rec.setValue("descriptor", income.descriptor);
    rec.setValue("owner", income.owner);

    if (!YearlyIncomeView::instance->update(income.date.year(), income.type, income.amount))
        return false;
    return insertRecord(-1, rec);
}

void OneTimeIncomeModel::new_dialog()
{
    CreateOneTimeIncomeDialog dialog{this};
    dialog.exec();
}

YearlyIncomeView * YearlyIncomeView::instance = nullptr;

YearlyIncomeView::YearlyIncomeView(QObject * parent, QSqlDatabase * db) : AbstractTableView(parent, db)
{
    this->setTable("yearly_income_view");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
    YearlyIncomeView::instance = this;
}

void YearlyIncomeView::set_record(QSqlRecord *rec, int year, const QString &type, double amt)
{
    rec->setValue("year", year);
    rec->setValue("Salary", 0);
    rec->setValue("Hourly", 0);
    rec->setValue("Rental", 0);
    rec->setValue("Dividend", 0);
    rec->setValue("Bond", 0);
    rec->setValue(type, amt);
}
