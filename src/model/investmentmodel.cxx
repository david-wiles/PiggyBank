#include "inc/model/investmentmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createinvestmentdialog.hxx>

SecurityInvestmentModel::SecurityInvestmentModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    this->setTable("investment");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool SecurityInvestmentModel::insert(const security_investment &investment)
{
    QSqlRecord rec = record();
    rec.setValue("type", investment.type);
    rec.setValue("ticker", investment.ticker);
    rec.setValue("buy_date", investment.buy_date.toString("yyyy-MM-dd"));
    rec.setValue("buy_price", investment.buy_price);
    rec.setValue("num_shares", investment.num_shares);
    rec.setValue("owner", investment.owner);
    return insertRecord(-1, rec);
}

QVector<QString> SecurityInvestmentModel::investment_types = {
        "Stock",
        "Bond",
        "Mutual Fund",
        "Real Estate",
        "Money Market"
};

void SecurityInvestmentModel::new_dialog()
{
    CreateInvestmentDialog dialog{this};
    dialog.exec();
}
