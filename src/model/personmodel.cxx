#include "inc/model/personmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createpersondialog.hxx>

PersonModel::PersonModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    this->setTable("person");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool PersonModel::insert(const person &person)
{
    QSqlRecord rec = record();
    rec.setValue("username", person.name);
    rec.setValue("first_name", person.first_name);
    rec.setValue("last_name", person.last_name);
    rec.setValue("age", person.age);
    return insertRecord(-1, rec);
}

void PersonModel::new_dialog()
{
    CreatePersonDialog dialog{this};
    dialog.exec();
}
