#include "inc/model/spendingmodel.hxx"

#include <QtSql/QSqlRecord>
#include <QtWidgets/QMessageBox>
#include <inc/view/createspendingdialog.hxx>


SpendingModel::SpendingModel(QObject *parent, QSqlDatabase * db) : AbstractModel(parent, db)
{
    this->setTable("spending");
    this->setEditStrategy(QSqlTableModel::OnManualSubmit);
}

bool SpendingModel::insert(const spending &spending)
{
    QSqlRecord rec = record();
    rec.setValue("date", spending.date.toString("yyyy-MM-dd"));
    rec.setValue("amount", spending.amount);
    rec.setValue("type", spending.type);
    rec.setValue("description", spending.description);
    rec.setValue("owner", spending.owner);
    return insertRecord(-1, rec);
}

QVector<QString> SpendingModel::spending_types = {
        "Food",
        "Entertainment",
        "Travel",
        "Sport",
        "Gas",
        "Education"
};

void SpendingModel::new_dialog()
{
    CreateSpendingDialog dialog{this};
    dialog.exec();
}
