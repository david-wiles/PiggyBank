#include "uservalidator.hxx"

UserValidator::UserValidator(QObject * parent, QSqlDatabase db) : QValidator(parent)
{
    person_model = new PersonModel(this, &db);
}


QValidator::State UserValidator::validate(QString &input, int &pos) const
{
    person_model->setFilter("name = " + input);
    person_model->select();

    if (person_model->rowCount() == 1)
        return QValidator::Acceptable;
    else
        return QValidator::Intermediate;
}

void UserValidator::fixup(QString &input) const
{
    QValidator::fixup(input);
}
