#include "inc/view/abstractcreatedialog.hxx"

#include <iostream>

#include <QtWidgets/QLabel>
#include <QtGui/QRegExpValidator>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QComboBox>


AbstractCreateDialog::AbstractCreateDialog(QWidget *parent)
        : QDialog(parent)
{
    button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(button_box, &QDialogButtonBox::accepted, this, &AbstractCreateDialog::accept);
    connect(button_box, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

void AbstractCreateDialog::accept()
{
    create_object();
    QDialog::accept();
}
