#include "inc/view/createaccountdialog.hxx"

#include <iostream>

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtGui/QRegExpValidator>

#include "uservalidator.hxx"


CreateAccountDialog::CreateAccountDialog(AccountModel * model, QWidget *parent)
        : AbstractCreateDialog(parent), account_model(model)
{
    setup();
}

CreateAccountDialog::~CreateAccountDialog()
{
    delete acct_num;
    delete acct_type;
    delete acct_balance;
    delete acct_interest;
    delete acct_institution;
    delete acct_owner;
}

void CreateAccountDialog::create_object()
{
    account a{
        this->acct_num->text(),
        this->acct_type->currentText(),
        this->acct_balance->value(),
        this->acct_interest->value(),
        this->acct_institution->text(),
        this->acct_owner->text(),
    };

    account_model->insert(a);
    account_model->submitAll();

    account_model->select();
}

void CreateAccountDialog::setup()
{
    auto acct_num_label = new QLabel(tr("Account number"));
    acct_num = new QLineEdit;
    acct_num->setValidator(new QRegExpValidator(QRegExp("[0-9]{12}")));

    auto acct_type_label = new QLabel(tr("Account type"));
    acct_type = new QComboBox;
    acct_type->addItem("Checking");
    acct_type->addItem("Savings");
    acct_type->addItem("Brokerage");
    acct_type->addItem("Retirement");

    auto balance_label = new QLabel(tr("Balance:"));
    acct_balance = new QDoubleSpinBox;
    acct_balance->setMaximum(1000000000);

    auto interest_label = new QLabel(tr("Interest"));
    acct_interest = new QDoubleSpinBox;

    auto bank_label = new QLabel(tr("Institution"));
    acct_institution = new QLineEdit;

    auto owner_label = new QLabel(tr("Owner"));
    acct_owner = new QLineEdit;
    acct_owner->setValidator(new UserValidator(this, account_model->database()));

    auto layout = new QVBoxLayout;
    layout->addWidget(acct_num_label);
    layout->addWidget(acct_num);
    layout->addWidget(acct_type_label);
    layout->addWidget(acct_type);
    layout->addWidget(balance_label);
    layout->addWidget(acct_balance);
    layout->addWidget(interest_label);
    layout->addWidget(acct_interest);
    layout->addWidget(bank_label);
    layout->addWidget(acct_institution);
    layout->addWidget(owner_label);
    layout->addWidget(acct_owner);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);

    setWindowTitle(tr("Create Account"));
}
