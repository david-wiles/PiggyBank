#include <QtWidgets/QLabel>
#include <inc/uservalidator.hxx>
#include <QtWidgets/QVBoxLayout>
#include "inc/view/createassetdialog.hxx"

CreateAssetDialog::CreateAssetDialog(AssetModel *model, QWidget *parent)
    : AbstractCreateDialog(parent), asset_model(model)
{
    setup();
}

CreateAssetDialog::~CreateAssetDialog()
{
    delete name;
    delete type;
    delete owner;
    delete value;
}

void CreateAssetDialog::create_object()
{
    asset a{
        this->name->text(),
        this->type->currentText(),
        this->owner->text(),
        this->value->value()
    };
    asset_model->insert(a);
    asset_model->submitAll();

    asset_model->select();
}

void CreateAssetDialog::setup()
{
    auto name_label = new QLabel(tr("Asset name"));
    name = new QLineEdit;

    auto type_label = new QLabel(tr("Asset type"));
    type = new QComboBox;
    type->addItem("Cash");
    type->addItem("House");
    type->addItem("Real Estate");
    type->addItem("Insurance");
    type->addItem("Jewlery");

    auto owner_label = new QLabel(tr("Asset owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, asset_model->database()));

    auto value_label = new QLabel(tr("Monetary value"));
    value = new QDoubleSpinBox;
    value->setMaximum(1000000000);

    auto layout = new QVBoxLayout;
    layout->addWidget(name_label);
    layout->addWidget(name);
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(value_label);
    layout->addWidget(value);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);

    setWindowTitle(tr("Create Asset"));
}