#include <QtWidgets/QLabel>
#include <inc/uservalidator.hxx>
#include <QtWidgets/QVBoxLayout>
#include "inc/view/createbudgetdialog.hxx"

CreateBudgetDialog::CreateBudgetDialog(BudgetModel *model, QWidget *parent)
    : AbstractCreateDialog(parent), budget_model(model)
{
    setup();
}

CreateBudgetDialog::~CreateBudgetDialog()
{
    delete type;
    delete target_amount;
    delete owner;
    delete description;
}

void CreateBudgetDialog::create_object()
{
    budget b{
        this->type->currentText(),
        this->target_amount->value(),
        this->owner->text(),
        this->description->text()
    };
    budget_model->insert(b);
    budget_model->submitAll();

    budget_model->select();
}

void CreateBudgetDialog::setup()
{
    auto type_label = new QLabel(tr("Budget type"));
    type = new QComboBox;
    type->addItem("Spending limit");
    type->addItem("Savings goal");
    type->addItem("Recurring expense");

    auto target_amount_label = new QLabel(tr("Target amount"));
    target_amount = new QDoubleSpinBox;
    target_amount->setMaximum(1000000000);

    auto owner_label = new QLabel(tr("Budget owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, budget_model->database()));

    auto description_label = new QLabel(tr("Description"));
    description = new QLineEdit;

    auto layout = new QVBoxLayout;
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(target_amount_label);
    layout->addWidget(target_amount);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(description_label);
    layout->addWidget(description);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);

    setWindowTitle(tr("Create budget"));

}
