#include "inc/view/createdebtdialog.hxx"

#include <iostream>

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtGui/QRegExpValidator>

#include <inc/uservalidator.hxx>


CreateDebtDialog::CreateDebtDialog(DebtModel * model, QWidget *parent)
        : AbstractCreateDialog(parent), debt_model(model)
{
    setup();

}

CreateDebtDialog::~CreateDebtDialog()
{
    delete principal;
    delete interest;
    delete type;
    delete start_date;
    delete maturity_date;
    delete description;
    delete owner;
}

void CreateDebtDialog::create_object()
{
    debt d{
        this->principal->value(),
        this->interest->value(),
        this->type->currentText(),
        this->start_date->date(),
        this->maturity_date->date(),
        this->description->text(),
        this->owner->text()
    };
    debt_model->insert(d);
    debt_model->submitAll();

    debt_model->select();
}

void CreateDebtDialog::setup()
{
    auto principal_label = new QLabel(tr("Principal amount"));
    principal = new QDoubleSpinBox;
    principal->setMaximum(1000000000);

    auto interest_label = new QLabel(tr("Interest"));
    interest = new QDoubleSpinBox;
    interest->setMaximum(1);
    auto interest_info = new QLabel(tr("Enter the yearly rate"));

    auto type_label = new QLabel(tr("Loan type"));
    type = new QComboBox;
    type->addItem("Personal");
    type->addItem("Mortgage");
    type->addItem("Credit");
    type->addItem("Car");
    type->addItem("Student");

    auto start_date_label = new QLabel(tr("Start date"));
    start_date = new QDateEdit;

    auto end_date_label = new QLabel(tr("End date"));
    maturity_date = new QDateEdit;

    auto description_label = new QLabel(tr("Description"));
    description = new QLineEdit;

    auto owner_label = new QLabel(tr("Owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, debt_model->database()));

    auto layout = new QVBoxLayout;
    layout->addWidget(principal_label);
    layout->addWidget(principal);
    layout->addWidget(interest_label);
    layout->addWidget(interest);
    layout->addWidget(interest_info);
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(start_date_label);
    layout->addWidget(start_date);
    layout->addWidget(end_date_label);
    layout->addWidget(maturity_date);
    layout->addWidget(description_label);
    layout->addWidget(description);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);
}
