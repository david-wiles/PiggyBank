#include "inc/view/createincomedialog.hxx"

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtGui/QRegExpValidator>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QComboBox>
#include <inc/uservalidator.hxx>


CreateRecurringIncomeDialog::CreateRecurringIncomeDialog(RecurringIncomeModel * model, QWidget *parent)
    : AbstractCreateDialog(parent), income_model(model)
{
    setup();
}

CreateRecurringIncomeDialog::~CreateRecurringIncomeDialog()
{
    delete type;
    delete hours;
    delete amount;
    delete pay_frequency;
    delete start_date;
    delete owner;
}

void CreateRecurringIncomeDialog::create_object()
{
    recurring_income income{
        this->type->currentText(),
        this->amount->value(),
        this->hours->value(),
        this->pay_frequency->value(),
        this->start_date->date(),
        this->end_date->date(),
        this->owner->text()
    };

    income_model->insert(income);
    income_model->submitAll();

    income_model->select();
}

void CreateRecurringIncomeDialog::setup()
{
    auto type_label = new QLabel(tr("Type"));
    type = new QComboBox;
    type->addItem("Salary");
    type->addItem("Hourly");
    type->addItem("Rental");

    auto amount_label = new QLabel(tr("Amount"));
    amount = new QDoubleSpinBox;
    amount->setMaximum(1000000000);

    auto hours_label = new QLabel(tr("Hours"));
    hours = new QDoubleSpinBox;
    hours->setMaximum(168);

    auto pay_freq_label = new QLabel(tr("Pay frequency"));
    pay_frequency = new QSpinBox;
    pay_frequency->setMaximum(365);

    auto start_date_label = new QLabel(tr("Start date"));
    start_date = new QDateEdit;
    auto end_date_label = new QLabel(tr("End date"));
    end_date = new QDateEdit;

    auto owner_label = new QLabel(tr("Owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, income_model->database()));

    auto layout = new QVBoxLayout;
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(amount_label);
    layout->addWidget(amount);
    layout->addWidget(new QLabel(tr("If hours is not applicable, leave field blank.")));
    layout->addWidget(hours_label);
    layout->addWidget(hours);
    layout->addWidget(pay_freq_label);
    layout->addWidget(pay_frequency);
    layout->addWidget(start_date_label);
    layout->addWidget(start_date);
    layout->addWidget(end_date_label);
    layout->addWidget(end_date);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);
}

CreateOneTimeIncomeDialog::CreateOneTimeIncomeDialog(OneTimeIncomeModel * model, QWidget *parent)
    : AbstractCreateDialog(parent), income_model(model)
{
    setup();
}

CreateOneTimeIncomeDialog::~CreateOneTimeIncomeDialog()
{
     delete type;
     delete amount;
     delete date;
     delete descriptor;
     delete owner;
}

void CreateOneTimeIncomeDialog::create_object()
{
    one_time_income income{
            this->type->currentText(),
            this->amount->value(),
            this->date->date(),
            this->descriptor->text(),
            this->owner->text()
    };

    income_model->insert(income);
    income_model->submitAll();

    income_model->select();

}

void CreateOneTimeIncomeDialog::setup()
{
    auto type_label = new QLabel(tr("Type"));
    type = new QComboBox;
    type->addItem("Dividend");

    auto amount_label = new QLabel(tr("Amount"));
    amount = new QDoubleSpinBox;
    amount->setMaximum(1000000000);

    auto date_label = new QLabel(tr("Start date"));
    date = new QDateEdit;

    auto descriptor_label = new QLabel(tr("Descriptor"));
    descriptor = new QLineEdit;

    auto owner_label = new QLabel(tr("Owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, income_model->database()));

    auto layout = new QVBoxLayout;
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(amount_label);
    layout->addWidget(amount);
    layout->addWidget(date_label);
    layout->addWidget(date);
    layout->addWidget(descriptor_label);
    layout->addWidget(descriptor);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);
}
