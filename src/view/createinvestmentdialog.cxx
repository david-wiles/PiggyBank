#include "inc/view/createinvestmentdialog.hxx"

#include <iostream>

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QComboBox>
#include <inc/uservalidator.hxx>


CreateInvestmentDialog::CreateInvestmentDialog(SecurityInvestmentModel * model, QWidget *parent)
    : AbstractCreateDialog(parent), investment_model(model)
{
    setup();
}

CreateInvestmentDialog::~CreateInvestmentDialog()
{
    delete type;
    delete ticker;
    delete buy_date;
    delete buy_price;
    delete num_shares;
    delete owner;
}

void CreateInvestmentDialog::create_object()
{
    security_investment investment{
        this->type->currentText(),
        this->ticker->text(),
        this->buy_date->date(),
        this->buy_price->value(),
        this->num_shares->value(),
        this->owner->text()
    };

    investment_model->insert(investment);
    investment_model->submitAll();

    investment_model->select();
}

void CreateInvestmentDialog::setup()
{
    auto type_label = new QLabel(tr("Security type"));
    type = new QComboBox;
    foreach (QString t, SecurityInvestmentModel::investment_types) {
        type->addItem(t);
    }

    auto ticker_label = new QLabel(tr("Ticker symbol"));
    ticker = new QLineEdit;

    auto buy_date_label = new QLabel(tr("Buy date"));
    buy_date = new QDateEdit;

    auto buy_price_label = new QLabel(tr("Buy price"));
    buy_price = new QDoubleSpinBox;
    buy_price->setMaximum(1000000000);

    auto num_shares_label = new QLabel(tr("Number of shares"));
    num_shares = new QDoubleSpinBox;
    num_shares->setMaximum(1000000000);

    auto owner_label = new QLabel(tr("Owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, investment_model->database()));

    auto layout = new QVBoxLayout;
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(ticker_label);
    layout->addWidget(ticker);
    layout->addWidget(buy_date_label);
    layout->addWidget(buy_date);
    layout->addWidget(buy_price_label);
    layout->addWidget(buy_price);
    layout->addWidget(num_shares_label);
    layout->addWidget(num_shares);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);
}
