#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include "inc/view/createpersondialog.hxx"

CreatePersonDialog::CreatePersonDialog(PersonModel *model, QWidget *parent)
    : AbstractCreateDialog(parent), person_model(model)
{
    setup();
}

CreatePersonDialog::~CreatePersonDialog()
{
    delete name;
    delete first_name;
    delete last_name;
    delete age;
}

void CreatePersonDialog::create_object()
{
    person p{
        this->name->text(),
        this->first_name->text(),
        this->last_name->text(),
        this->age->value()
    };
    person_model->insert(p);
    person_model->submitAll();

    person_model->select();
}

void CreatePersonDialog::setup()
{
    auto name_label = new QLabel(tr("Name"));
    name = new QLineEdit;

    auto first_name_label = new QLabel(tr("First Name"));
    first_name = new QLineEdit;

    auto last_name_label = new QLabel(tr("Last Name"));
    last_name = new QLineEdit;

    auto age_label = new QLabel(tr("Age"));
    age = new QSpinBox;
    age->setMinimum(0);

    auto layout = new QVBoxLayout;
    layout->addWidget(name_label);
    layout->addWidget(name);
    layout->addWidget(first_name_label);
    layout->addWidget(first_name);
    layout->addWidget(last_name_label);
    layout->addWidget(last_name);
    layout->addWidget(age_label);
    layout->addWidget(age);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);
    setWindowTitle(tr("Create person"));
}


