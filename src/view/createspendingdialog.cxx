#include "inc/view/createspendingdialog.hxx"

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtGui/QDoubleValidator>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QComboBox>
#include <inc/uservalidator.hxx>

CreateSpendingDialog::CreateSpendingDialog(SpendingModel * model, QWidget *parent)
    : AbstractCreateDialog(parent), spending_model(model)
{
    setup();
}

CreateSpendingDialog::~CreateSpendingDialog()
{
    delete date;
    delete amount;
    delete type;
    delete description;
    delete owner;
}

void CreateSpendingDialog::create_object()
{
    spending s{
        this->date->date(),
        this->amount->value(),
        this->type->currentText(),
        this->description->text(),
        this->owner->text()
    };

    spending_model->insert(s);
    spending_model->submitAll();

    spending_model->select();
}

void CreateSpendingDialog::setup()
{
    auto date_label = new QLabel(tr("Date of spending"));
    date = new QDateEdit;

    auto amt_label = new QLabel(tr("Amount"));
    amount = new QDoubleSpinBox;
    amount->setMaximum(1000000000);

    auto type_label = new QLabel(tr("Spending type"));
    type = new QComboBox;
    foreach (QString t, SpendingModel::spending_types) {
        type->addItem(t);
    }

    auto description_label = new QLabel(tr("Description"));
    description = new QLineEdit;

    auto owner_label = new QLabel(tr("Owner"));
    owner = new QLineEdit;
    owner->setValidator(new UserValidator(this, spending_model->database()));

    auto layout = new QVBoxLayout;
    layout->addWidget(date_label);
    layout->addWidget(date);
    layout->addWidget(amt_label);
    layout->addWidget(amount);
    layout->addWidget(type_label);
    layout->addWidget(type);
    layout->addWidget(description_label);
    layout->addWidget(description);
    layout->addWidget(owner_label);
    layout->addWidget(owner);
    layout->addWidget(button_box);

    layout->addStretch(1);
    setLayout(layout);
}